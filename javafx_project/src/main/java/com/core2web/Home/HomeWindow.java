package com.core2web.Home;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HomeWindow extends Application{
    Text txt1 = null;
    Text txt2 = null; 
    Text title = null;
    Text java = null;
    Text cpp = null;
    Text python = null;
    Text web = null;
    Text backend = null;
    Text app = null;

    @Override
    public void start(Stage prStage) {
      prStage.setTitle("MyStage");
      prStage.setHeight(800);
      prStage.setWidth(1000);
      prStage.setResizable(false);
      prStage.getIcons().add(new Image("images/krishna.jpeg"));

      title = new Text(40,50,"Programming Languages");
      title.setFont(new Font(40));

      java = new Text(40,150,"java");
      python = new Text(40,170,"python");
      cpp = new Text(40,190,"cpp");
      web = new Text(90,150,"web");
      backend = new Text(90,170,"backend");
      app = new Text(90,190,"app");
      txt1 = new Text(10,40,"Good Evening");
      txt2 = new Text(400,40,"Have a nice day");
      txt1.setFill(Color.BLUE);
      java.setFill(Color.INDIGO);
      python.setFill(Color.GREEN);
      cpp.setFill(Color.MAROON);
      txt2.setFill(Color.BLUE);
      txt1.setFont(new Font(30));
      txt2.setFont(new Font(30));
      java.setFont(new Font(20));
      python.setFont(new Font(20));
      cpp.setFont(new Font(20));
      app.setFont(new Font(20));
      web.setFont(new Font(20));

      VBox vb1 = new VBox(40,java,python,cpp);
      vb1.setLayoutX(100);
      vb1.setLayoutY(120);

      VBox vb2 = new VBox(40,web,backend,app);
      vb2.setLayoutX(190);
      vb2.setLayoutY(120);

      HBox hb = new HBox(50,vb1,vb2);
      hb.setLayoutX(150);
      hb.setLayoutY(160);


      Group group = new Group(txt1,txt2,hb);
      Scene sc = new Scene(group);
      sc.setFill(Color.BURLYWOOD);
      prStage.setScene(sc);

      prStage.show();
}
}