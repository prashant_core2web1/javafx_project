package com.core2web.Home;

import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyPane extends Application {
     @Override
    public void start(Stage primaryStage){
    primaryStage.setTitle("MyProject");
    primaryStage.setHeight(800);
    primaryStage.setWidth(1000);
    primaryStage.setResizable(true);
    primaryStage.getIcons().add(new Image("images/krishna.jpeg"));
//****************************************Pane********************************************
    Text txt = new Text(925, 80, "MyFirstProject");
    txt.setFont(new Font(20));
    txt.setUnderline(true);
    txt.setFill(Color.BLACK);

    Label lb = new Label("Core2Web");
    lb.setTextFill(Color.RED);
    lb.setFont(new Font(45));
    lb.setLayoutX(900);
    Pane root = new Pane();
    root.getChildren().addAll(lb);   
//*************************************BorderPane**************************************
    Label top = new Label("Top");
    top.setFont(new Font(40));
    Label left= new Label("Left");
    left.setFont(new Font(40)); 
    Label right = new Label("Right");
    right.setFont(new Font(40));
    Label bottom = new Label("Bottom");
    bottom.setFont(new Font(40));
    Label center = new Label("Center");
    center.setFont(new Font(40));

    BorderPane bp = new BorderPane();
    bp.setTop(top);
    bp.setLeft(left);
    bp.setRight(right);
    bp.setBottom(bottom);
    bp.setCenter(center);
//*******************************************StackPane*****************************
    Label l1 = new Label("Talk is Cheap Show me the Code");
    l1.setFont(new Font(40));
    l1.setTextFill(Color.BLACK);

    Label l2 = new Label("--------------                               ");
    l2.setFont(new Font(40));
    l2.setTextFill(Color.RED);

    StackPane stackPane = new StackPane();
    stackPane.getChildren().addAll(l1,l2);
//********************************************Scene***********************************************
    Group gp = new Group(txt,root);

    Scene sc = new Scene(gp,800,200);
   // Scene sc = new Scene(stackPane,800,200);
    sc.setFill(Color.INDIGO);
    sc.setCursor(Cursor.CLOSED_HAND);

    primaryStage.setTitle("StackPane");
    primaryStage.setScene(sc);
    primaryStage.show();
    }
}
