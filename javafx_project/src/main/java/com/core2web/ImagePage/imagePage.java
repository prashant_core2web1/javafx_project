package com.core2web.ImagePage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class imagePage extends Application{
      public void start(Stage primaryStage){
      primaryStage.setTitle("ImagePane");
      primaryStage.setHeight(800);
      primaryStage.setWidth(1000);
      primaryStage.setResizable(true);
      primaryStage.getIcons().add(new Image("assets/image/krishna.jpeg"));
      
      Label lb = new Label("Shri Krishna");
      lb.setFont(new Font(30));
      lb.setAlignment(Pos.CENTER);
      lb.setPrefWidth(300);
      lb.setPrefHeight(400);
      
      Image img = new Image("assets/image/krishna.jpeg");
      ImageView iv = new ImageView(img);
      iv.setFitHeight(400);
      iv.setFitWidth(500);
      iv.setPreserveRatio(true);

      VBox vb = new VBox(iv);
    
      VBox vb1 = new VBox(lb);
      vb1.setAlignment(Pos.CENTER);
      vb1.prefWidth(500);
      vb1.prefHeight(100);

      HBox hb = new HBox(30,vb,vb1);
      hb.setStyle("-fx-background-color:Green");
      hb.prefHeight(300);
      hb.prefWidth(400);

      Group gp = new Group(hb);
      Scene sc = new Scene(gp,800,200);
      sc.setFill(Color.CADETBLUE);

      primaryStage.setScene(sc);
      primaryStage.show();
    }
}
