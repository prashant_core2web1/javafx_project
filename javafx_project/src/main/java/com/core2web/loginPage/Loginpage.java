package com.core2web.loginPage;



import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Loginpage extends Application {
    
      @Override
    public void start(Stage loginStage){

     loginStage.setTitle("Loginpage");
     loginStage.setHeight(800);
     loginStage.setWidth(800);
     loginStage.setResizable(false);
     loginStage.getIcons().add(new Image("images/image.jpeg"));
    /**************************************************************************************/ 
     Image img = new Image("images/userIcon.png");
     ImageView iv = new ImageView(img);
     iv.setFitWidth(200);
     iv.setFitHeight(150);
     VBox vb = new VBox(iv);
     vb.setPrefHeight(30);
     vb.setPrefWidth(100);
     vb.setLayoutX(300);
     Label headline = new Label("Login");
     headline.setFont(new Font(40));

     HBox labelHBox = new HBox(headline);
     labelHBox.setPrefHeight(400);
     labelHBox.setPrefWidth(400);
     labelHBox.setLayoutX(200);
     labelHBox.setAlignment(Pos.CENTER);


     Label emaillabel = new Label("Enter your email id : ");
     emaillabel.setFont(new Font(20));
     TextField emailTextField = new TextField();
     VBox emailVBox = new VBox(emaillabel,emailTextField);
     

     Label passlabel = new Label("Enter password : ");
     passlabel.setFont(new Font(20));
     PasswordField passField = new PasswordField();
     VBox passVBox= new VBox(passlabel,passField);

     Label lb1 = new Label();
     lb1.setFont(new Font(20));
     Label lb2 = new Label();
     HBox hbl1 = new HBox(lb2);
     hbl1.setPrefWidth(400);
     hbl1.setPrefHeight(30);
     hbl1.setAlignment(Pos.CENTER_LEFT);

     Label lb3 = new Label();
     lb3.setFont(new Font(20));
     Label lb4 = new Label();
     HBox hbl2 = new HBox(lb4);
     hbl2.setPrefWidth(400);
     hbl2.setPrefHeight(30);
     hbl2.setAlignment(Pos.CENTER_LEFT);

     Button showbutton = new Button("Submit");
     showbutton.setPrefWidth(200);
     showbutton.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
           System.out.println(emailTextField.getText());
           System.out.println(passField.getText());
           lb1.setText("Email-ID :");
           lb2.setText(emailTextField.getText());
           lb3.setText("Password :");
           lb4.setText(passField.getText());
           hbl1.setStyle("-fx-background-color:Cyan");
           hbl2.setStyle("-fx-background-color:Cyan");
        }
    });
    
    HBox hb = new HBox(showbutton);
    hb.setPrefWidth(400);
    hb.setAlignment(Pos.CENTER);
    
    VBox vb1 = new VBox(20,emailVBox,passVBox,hb);
    vb1.setPrefHeight(160);
    vb1.setPrefWidth(400);
    vb1.setLayoutX(200);
    vb1.setLayoutY(250);

    VBox vbl1 = new VBox(lb1,hbl1);
    VBox vbl2 = new VBox(lb3,hbl2);
    VBox vb2 = new VBox(20,vbl1,vbl2);
    vb2.setPrefWidth(400);
    vb2.setLayoutX(200);
    vb2.setLayoutY(450);
    vb2.setStyle("-fx-background-image:images/images.jpeg ;");
    vb2.setAlignment(Pos.CENTER);

    Group gp = new Group(labelHBox,vb1,vb2);

    Scene sc = new Scene(gp);
    sc.setFill(Color.HOTPINK);
    loginStage.setScene(sc);
    loginStage.show();
    }
}
