package com.core2web.imageassign;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class imageassign extends Application {
    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("ImageAssigement");
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1800);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("images/Krishna.jpeg"));
        
        Labeled title = new Label("Languages");
        title.setFont(new Font(60));
        title.setLayoutX(750);
        title.setTextFill(Color.ORCHID);
        HBox hbTitle = new HBox(title);
        hbTitle.setPrefHeight(60);
        hbTitle.setPrefWidth(1800);
        hbTitle.setAlignment(Pos.CENTER);
        
      //******************************************************************************************************/  
        Label lb1= new Label("Java");
        lb1.setFont(new Font(30));
        //lb3.setPrefHeight(200);
        //lb3.setPrefWidth(500);
        // lb3.setAlignment(Pos.CENTER);

        Image img1 = new Image("assets/image/javaIcon.png");
        ImageView iv1 = new ImageView(img1);
        iv1.setFitHeight(150);
        iv1.setFitWidth(200);
        iv1.setPreserveRatio(true);
         
        VBox vb1 = new VBox(iv1);
        vb1.setPrefHeight(150);
        vb1.setPrefWidth(200);
        vb1.setAlignment(Pos.CENTER);

        VBox vb2 = new VBox(lb1);
        vb2.setPrefHeight(150);
        vb2.setPrefWidth(400);
        vb2.setAlignment(Pos.CENTER);
       

        HBox hb1 = new HBox(vb1,vb2);
        hb1.setStyle("-fx-background-color:White");
        vb2.setStyle("-fx-background-color:Blue");

      //*********************************************************************************************************/
        Label lb2 = new Label("Python");
        lb2.setFont(new Font(30));
        //lb2.setPrefHeight(200);
        //lb2.setPrefWidth(500);
        //lb2.setAlignment(Pos.CENTER);

        Image img2 = new Image("assets/image/pythonIcon.jpg");
        ImageView iv2 = new ImageView(img2);
        iv2.setFitHeight(150);
        iv2.setFitWidth(200);
        iv2.setPreserveRatio(true);

        VBox vb3 = new VBox(iv2);
        vb3.setPrefHeight(150);
        vb3.setPrefWidth(200);
        vb3.setAlignment(Pos.CENTER);

        VBox vb4 = new VBox(lb2);
        vb4.setPrefHeight(150);
        vb4.setPrefWidth(400);
        vb4.setAlignment(Pos.CENTER);
        

        HBox hb2 = new HBox(vb3,vb4);
        hb2.setStyle("-fx-background-color:White");
        vb4.setStyle("-fx-background-color:Yellow");

      //*****************************************************************************************************/
        
 
        Label lb3= new Label("C");
        lb3.setFont(new Font(30));
        //lb3.setPrefHeight(200);
        //lb3.setPrefWidth(500);
        // lb3.setAlignment(Pos.CENTER);

        Image img3 = new Image("assets/image/CIcon.png");
        ImageView iv3 = new ImageView(img3);
        iv3.setFitHeight(150);
        iv3.setFitWidth(200);
        iv3.setPreserveRatio(true);
         
        VBox vb5 = new VBox(iv3);
        vb5.setPrefHeight(150);
        vb5.setPrefWidth(200);
        vb5.setAlignment(Pos.CENTER);

        VBox vb6 = new VBox(lb3);
        vb6.setPrefHeight(150);
        vb6.setPrefWidth(400);
        vb6.setAlignment(Pos.CENTER);
        
        HBox hb3 = new HBox(vb5,vb6);
        hb3.setStyle("-fx-background-color:White");
        vb6.setStyle("-fx-background-color:Pink");

      //****************************************************************************************************/

        Label lb4= new Label("CPP");
        lb4.setFont(new Font(30));
        
        Image img4 = new Image("assets/image/cppIcon.png");
        ImageView iv4 = new ImageView(img4);
        iv4.setFitHeight(150);
        iv4.setFitWidth(200);
        iv4.setPreserveRatio(true);
         
        VBox vb7 = new VBox(iv4);
        vb7.setPrefHeight(150);
        vb7.setPrefWidth(200);
        vb7.setAlignment(Pos.CENTER);

        VBox vb8 = new VBox(lb4);
        vb8.setPrefHeight(150);
        vb8.setPrefWidth(400);
        vb8.setAlignment(Pos.CENTER);
        

        HBox hb4 = new HBox(vb7,vb8);
        hb4.setStyle("-fx-background-color:White");
        vb8.setStyle("-fx-background-color: Green");

      //************************************************************************************************************/
        
      Label lb5= new Label("Spring");
      lb5.setFont(new Font(30));
      
      Image img5 = new Image("assets/image/springlogo.png");
      ImageView iv5 = new ImageView(img5);
      iv5.setFitHeight(150);
      iv5.setFitWidth(200);
      iv5.setPreserveRatio(true);
       
      VBox vb9 = new VBox(iv5);
      vb9.setPrefHeight(150);
      vb9.setPrefWidth(200);
      vb9.setAlignment(Pos.CENTER);

      VBox vb10 = new VBox(lb5);
      vb10.setPrefHeight(150);
      vb10.setPrefWidth(400);
      vb10.setAlignment(Pos.CENTER);
      

      HBox hb5 = new HBox(vb9,vb10);
      hb5.setStyle("-fx-background-color:White");
      vb10.setStyle("-fx-background-color:Blue");

    //*******************************************************************************************************/  

      Label lb6= new Label("React");
      lb6.setFont(new Font(30));
    
      Image img6 = new Image("assets/image/ReactIcon.png");
      ImageView iv6 = new ImageView(img6);
      iv6.setFitHeight(150);
      iv6.setFitWidth(200);
      iv6.setPreserveRatio(true);
     
      VBox vb11 = new VBox(iv6);
      vb11.setPrefHeight(150);
      vb11.setPrefWidth(200);
      vb11.setAlignment(Pos.CENTER);

      VBox vb12 = new VBox(lb6);
      vb12.setPrefHeight(150);
      vb12.setPrefWidth(400);
      vb12.setAlignment(Pos.CENTER);
      

      HBox hb6 = new HBox(vb11,vb12);
      hb6.setStyle("-fx-background-color:White");
      vb12.setStyle("-fx-background-color: Yellow");

      //***************************************************************************************************/

      Label lb7= new Label("Flutter");
      lb7.setFont(new Font(30));
    
      Image img7 = new Image("assets/image/flutterIcon.jpg");
      ImageView iv7 = new ImageView(img7);
      iv7.setFitHeight(150);
      iv7.setFitWidth(200);
      iv7.setPreserveRatio(true);
     
      VBox vb13 = new VBox(iv7);
      vb13.setPrefHeight(150);
      vb13.setPrefWidth(200);
      vb13.setAlignment(Pos.CENTER);

      VBox vb14 = new VBox(lb7);
      vb14.setPrefHeight(150);
      vb14.setPrefWidth(400);
      vb14.setAlignment(Pos.CENTER);
      

      HBox hb7 = new HBox(vb13,vb14);
      hb7.setStyle("-fx-background-color:White");
      vb14.setStyle("-fx-background-color: Pink");

      //*****************************************************************************************************/

      Label lb8= new Label("NodeJS");
      lb8.setFont(new Font(30));
    
      Image img8 = new Image("assets/image/NodeJSIcon.png");
      ImageView iv8 = new ImageView(img8);
      iv8.setFitHeight(150);
      iv8.setFitWidth(200);
      iv8.setPreserveRatio(true);
     
      VBox vb15 = new VBox(iv8);
      vb15.setPrefHeight(150);
      vb15.setPrefWidth(200);
      vb15.setAlignment(Pos.CENTER);

      VBox vb16 = new VBox(lb8);
      vb16.setPrefHeight(150);
      vb16.setPrefWidth(400);
      vb16.setAlignment(Pos.CENTER);
      
      HBox hb8 = new HBox(vb15,vb16);
      hb8.setStyle("-fx-background-color:White");
      vb16.setStyle("-fx-background-color: Green");

      //***************************************************************************************************/

        VBox vbox1 = new VBox(25,hb1,hb2,hb3,hb4);
        vbox1.setPrefHeight(675);
        vbox1.setPrefWidth(600);

        VBox vbox2 = new VBox(25,hb5,hb6,hb7,hb8);
        vbox2.setPrefHeight(675);
        vbox2.setPrefWidth(600);

        HBox hb = new HBox(25,vbox1,vbox2);
        hb.setPrefHeight(675);
        hb.setPrefWidth(1225);
        hb.setStyle("-fx-background-color:Orange");
        hb.setLayoutY(132);
        hb.setLayoutX(275);

        Group gp = new Group(title,hb);

        Scene sc = new Scene(gp);
        sc.setCursor(Cursor.CLOSED_HAND);
        sc.setFill(Color.BLACK);
        primaryStage.setScene(sc);
        primaryStage.show();
    
    }
}
