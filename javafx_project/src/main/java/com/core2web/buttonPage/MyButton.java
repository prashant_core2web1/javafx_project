package com.core2web.buttonPage;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyButton extends Application{
     @Override
        public void start(Stage primaryStage){
            primaryStage.setTitle("Buttons");
            primaryStage.getIcons().add(new Image("assets/image/krishna.jpeg"));
            primaryStage.setHeight(1000);
            primaryStage.setWidth(1000);
            primaryStage.setResizable(false);

            Image img = new Image("assets/image/userIcon.png");
            ImageView iv = new ImageView(img);
            iv.setFitWidth(250);
            iv.setFitHeight(200);
            
            Text txt = new Text("Username : ");
            TextField tx = new TextField();
            tx.setPrefWidth(200);
            tx.setAlignment(Pos.CENTER);

            Text txt1 = new Text("Password : ");
            PasswordField ps = new PasswordField();
            ps.setPrefWidth(200);
            ps.setAlignment(Pos.CENTER);
            
            VBox vb1 = new VBox(15,iv,txt,tx,txt1,ps);
            vb1.setPrefWidth(500);
            vb1.setAlignment(Pos.CENTER);

            HBox hb = new HBox(vb1);
            hb.setPrefWidth(1000);
            hb.setPrefHeight(300);
            hb.setAlignment(Pos.CENTER);
            
            Label lb1 = new Label();
            lb1.setFont(new Font(30));
            Label lb2 = new Label();
            lb2.setFont(new Font(30));

            Button show = new Button("Show");
            show.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    System.out.println(tx.getText());
                    System.out.println(ps.getText());
                    lb1.setText(tx.getText());
                    lb2.setText(ps.getText());
                }
                
            });
            
            VBox vb = new VBox(25,hb,show);
            vb1.setPrefWidth(600);
            vb.setAlignment(Pos.CENTER);
            
            VBox vbox = new VBox(24,lb1,lb2);
            vbox.setLayoutY(600);
            vbox.setLayoutX(450);


            Group gp = new Group(vb,vbox);
            Scene sc = new Scene(gp);
            sc.setFill(Color.HOTPINK);

            primaryStage.setScene(sc);
            primaryStage.show();
        }
}
