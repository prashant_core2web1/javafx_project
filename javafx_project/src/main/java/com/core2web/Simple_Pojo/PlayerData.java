package com.core2web.Simple_Pojo;

public class PlayerData {
    private String playerName;
    private String country;
    private int age;
                                                                                                                                                                                     
    public String getPlayerName() {
        return playerName;
    }
    public String getCountry() {
        return country;
    }
    public int getAge() {
        return age;
    }
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public void setAge(int age) {
        this.age = age;
    }
}

