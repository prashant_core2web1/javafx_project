package com.core2web.buttonAssign3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


public class buttonassign extends Application {
     @Override
    public void start(Stage primaryStage){
      
       primaryStage.setTitle("Core2web");
       primaryStage.setHeight(800);
       primaryStage.setWidth(1000);
       primaryStage.setResizable(false);
       primaryStage.getIcons().add(new Image("images/images.jpeg"));
       
       Button january = new Button("January");
       january.setPrefWidth(200);
       january.setLayoutX(500);
       january.setAlignment(Pos.CENTER);

       Button february = new Button("February");
       february.setPrefWidth(200);
       february.setLayoutX(500);
       february.setAlignment(Pos.CENTER);

       Button march = new Button("March");
       march.setPrefWidth(200);
       march.setLayoutX(500);
       march.setAlignment(Pos.CENTER);

       Button april = new Button("April");
       april.setPrefWidth(200);
       april.setLayoutX(500);
       april.setAlignment(Pos.CENTER);

       Button may = new Button("May");
       may.setPrefWidth(200);
       may.setLayoutX(500);
       may.setAlignment(Pos.CENTER);

       Button june = new Button("June");
       june.setPrefWidth(200);
       june.setLayoutX(500);
       june.setAlignment(Pos.CENTER);

       Button july = new Button("July");
       july.setPrefWidth(200);
       july.setLayoutX(500);
       july.setAlignment(Pos.CENTER);

       Button august = new Button("August");
       august.setPrefWidth(200);
       august.setLayoutX(500);
       august.setAlignment(Pos.CENTER);

       Button september = new Button("September");
       september.setPrefWidth(200);
       september.setLayoutX(500);
       september.setAlignment(Pos.CENTER);

       Button october = new Button("October");
       october.setPrefWidth(200);
       october.setLayoutX(500);
       october.setAlignment(Pos.CENTER);

       Button november = new Button("November");
       november.setPrefWidth(200);
       november.setLayoutX(500);
       november.setAlignment(Pos.CENTER);

       Button december = new Button("December");
       december.setPrefWidth(200);
       december.setLayoutX(500);
       december.setAlignment(Pos.CENTER);
       
       january.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       february.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       march.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       april.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       may.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       june.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       july.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       august.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       september.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       october.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       november.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
       december.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
          
       
       VBox vb = new VBox(20,january,february,march,april,may,june,july,august,september,october,november,december);
       vb.setPrefWidth(500);
       vb.setLayoutX(250);
       vb.setLayoutY(60);
       vb.setAlignment(Pos.CENTER);

      january.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("January");
            january.setStyle("-fx-background-color:Orange; -fx-font-weight:bold");
        }
     });
     february.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("February");
            february.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     march.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("March");
            march.setStyle("-fx-background-color:Orange; -fx-font-weight:bold");
        }
     });
     april.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("April");
            april.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     may.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("May");
            may.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     june.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("June");
            june.setStyle("-fx-background-color:Orange; -fx-font-weight:bold");
        }
     });
     july.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("July");
            july.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     august.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("August");
            august.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     september.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("September");
            september.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     october.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("October");
            october.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     november.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("November");
            november.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     december.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("December");
            december.setStyle("-fx-background-color:orange; -fx-font-weight:bold");
        }
     });
     //Group gp = new Group(vb);
    //**********************************************************************************************************/
       Label lb = new Label("Content :");
       lb.setAlignment(Pos.CENTER);
       Font boldBlackFont1 = Font.font("Arial",FontWeight.BOLD, 18);
       lb.setFont(boldBlackFont1);
       TextField tx = new TextField();
       tx.setPrefWidth(200);
       tx.setAlignment(Pos.CENTER);

       Button show = new Button("Print Text");
       show.setPrefWidth(200);
       Font boldBlackFont = Font.font("Arial",FontWeight.BOLD, 22);
       show.setFont(boldBlackFont);
       show.setStyle("-fx-background-color:Pink");

       Label lb1 = new Label();
       lb1.setFont(new Font(20));
       Font boldBlackFont2 = Font.font("Arial",FontWeight.BOLD, 24);
       lb1.setFont(boldBlackFont2);
       lb1.setLayoutY(600);
       show.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            System.out.println(tx.getText());
            lb1.setText(tx.getText());
        }
        
       });

       VBox vbb = new VBox(10,lb,tx,show);
       VBox vbb1 = new VBox(lb1);
       vbb.setPrefWidth(300);
       vbb.setAlignment(Pos.CENTER);

       vbb1.setPrefWidth(300);
       vbb1.setLayoutX(350);
       vbb1.setLayoutY(400);
       vbb1.setAlignment(Pos.CENTER);

       HBox hb = new HBox(vbb);
       hb.setLayoutX(350);
       hb.setLayoutY(150);
       hb.setPrefHeight(150);
       hb.setAlignment(Pos.CENTER);
      // Group gp = new Group(hb,vbb1);
    //*****************************************************************************************************/
       TextField txsquar = new TextField();
       HBox hbtxsquarHBox = new HBox(txsquar);
       hbtxsquarHBox.setPrefWidth(250);
       hbtxsquarHBox.setAlignment(Pos.CENTER);
  
       Button square = new Button("Square");
       square.setPrefWidth(300);
       //square.setPrefWidth(300);
       //square.setStyle("-fx-background-color: blue; -fx-font-weight: bold");
       Font squFont = Font.font("Arial",FontWeight.BOLD, 18);
       square.setFont(squFont);
       square.setStyle("-fx-background-color:Blue");

       Button squareroot = new Button("SquareRoot");
       squareroot.setPrefWidth(300);
       Font squRootFont = Font.font("Arial",FontWeight.BOLD, 18);
       squareroot.setFont(squRootFont);
       squareroot.setStyle("-fx-background-color:Blue");
       
       Label sqLabel = new Label();
       sqLabel.setFont(new Font(20)); 
       sqLabel.setTextFill(Color.INDIGO);
       sqLabel.setAlignment(Pos.CENTER);

       Label sqLabel2 = new Label();
       sqLabel2.setFont(new Font(20));
       sqLabel2.setTextFill(Color.INDIGO);
       
       square.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {

          int number = Integer.parseInt(txsquar.getText());
          int square = number * number;
          System.out.println("Square of the given number is --> "+square);
          sqLabel.setText("Square of the given number is --> "+String.valueOf(square));      
        }
       });
       
       squareroot.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
         
          int num = Integer.parseInt(txsquar.getText());
          int start = 1;
          int end = num;
          int mid = 0;
          int root =0;
          while(start<=end){
            mid = (start + end)/2;
            if((mid*mid) > num) {
               end = mid-1;
            }else if((mid*mid)<num) {
               root = mid;
               start = mid + 1;
            }else {
               root = mid;
               break;
           }
        }
          System.out.println("Square root of the given number is --> "+root);
          sqLabel2.setText("Square root of the given number is --> "+String.valueOf(root));
          
        }
        
       });
      

       VBox sqVBox = new VBox(15,txsquar,square,squareroot);
      // sqVBox.setPrefWidth(300);
       sqVBox.setLayoutX(350);
       sqVBox.setLayoutY(300);
       sqVBox.setAlignment(Pos.CENTER);

       VBox sqVBox1 = new VBox(sqLabel,sqLabel2);
       //sqVBox1.setPrefWidth(500);
       sqVBox1.setLayoutY(500);
       sqVBox1.setLayoutX(250);
       sqVBox1.setAlignment(Pos.CENTER);

       //Group gp = new Group(sqVBox,sqVBox1);
      //*******************************************************************************************************/
       PasswordField ps = new PasswordField();
       ps.setAlignment(Pos.CENTER);
       HBox hbpassHBox = new HBox(ps);
       hbpassHBox.setAlignment(Pos.CENTER);
       
       Label lbpass = new Label();
       lbpass.setAlignment(Pos.CENTER);
       
       Button showpass = new Button("Print Password");
       showpass.setAlignment(Pos.CENTER);
       showpass.setStyle("-fx-background-Color:Blue");
       showpass.setOnAction(new EventHandler<ActionEvent>() {

         @Override
         public void handle(ActionEvent event) {
            System.out.println(ps.getText());
            lbpass.setText(ps.getText());
            showpass.setStyle("-fx-background-color:Blue; -fx-font-weight:bold");
         }
          
       });
       VBox vbpass = new VBox(10,hbpassHBox,showpass);
       vbpass.setLayoutX(300);
       vbpass.setPrefWidth(400);
       vbpass.setLayoutY(300);
       vbpass.setAlignment(Pos.CENTER);

       VBox vblabel = new VBox(lbpass);
       vblabel.setLayoutX(300);
       vblabel.setPrefWidth(400);
       vblabel.setLayoutY(500);
       vblabel.setAlignment(Pos.CENTER);

       //Group gp = new Group(vbpass,vblabel);

       //*********************************************************************************/
       Label txlabel = new Label("Username :");
       txlabel.setStyle("-fx-font-weight:Bold");
       TextField txlogin = new TextField();
       txlogin.setAlignment(Pos.CENTER);

       Label psLabel = new Label("Password : ");
       psLabel.setStyle("-fx-font-weight:Bold");
       PasswordField pslogin = new PasswordField();
       pslogin.setAlignment(Pos.CENTER);

       String str1 ="Prashant";
       String str2 = "pass@123";

       Label lbpassLabel = new Label();
       lbpassLabel.setTextFill(Color.RED);
       lbpassLabel.setStyle("-fx-font-weight:Bold");
       lbpassLabel.setAlignment(Pos.CENTER);

       Button button = new Button("Login");
       button.setPrefWidth(200);
       button.setStyle("-fx-background-color:Blue ; -fx-font-weight:bold");
       button.setAlignment(Pos.CENTER);

       button.setOnAction(new EventHandler<ActionEvent>(){

         public void handle(ActionEvent event) {
            System.out.println(txlogin.getText());
            System.out.println(pslogin.getText());
            if(str1.equals(txlogin.getText()) && str2.equals(pslogin.getText())){
               lbpassLabel.setText("You are Welcome.....");

            }
            else{
               lbpassLabel.setText("Invalid Username or Password.");
            }

         }
         
       });
       
       VBox vbloginVBox = new VBox(15,txlabel,txlogin,psLabel,pslogin,button,lbpassLabel);
       vbloginVBox.setLayoutX(350);
       vbloginVBox.setLayoutY(250);
       vbloginVBox.setPrefWidth(300);
       vbloginVBox.setAlignment(Pos.CENTER);
       
       //Group gp = new Group(vbloginVBox);
       //***********************************************************************************************/
       TextField txnum = new TextField();
       txnum.setAlignment(Pos.CENTER);
       
       Button shownum = new Button("Check Palindrome");
       shownum.setAlignment(Pos.CENTER);
       shownum.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");
       shownum.setPrefWidth(300);
       shownum.setTextFill(Color.RED);

       Label palindrome = new Label();
       palindrome.setFont(new Font(15));
       palindrome.setTextFill(Color.BLACK);
       palindrome.setStyle("-fx-font-weight:Bold");
       palindrome.setPrefWidth(300);
       palindrome.setAlignment(Pos.CENTER);



       shownum.setOnAction(new EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
           int number = Integer.parseInt(txnum.getText());
           int rev=0;
           int rem=0;
           int temp=number;
            while(temp>0){
               rem = temp % 10;
               rev = rev * 10 + rem;
               temp = temp/ 10;
           }
           if(number == rev){
            palindrome.setText("Palindrome Number"); 
           }
           else{
            palindrome.setText("Not Palindrome Number "); 
           }
          
         }
         
       });
          
        VBox vbpalindrome = new VBox(20,txnum,shownum,palindrome);
        vbpalindrome.setAlignment(Pos.CENTER);
        vbpalindrome.setPrefWidth(300);
        vbpalindrome.setLayoutX(350);
        vbpalindrome.setLayoutY(250);
        
        Group gp = new Group(vbpalindrome);

       //************************************************************************************************/
       Button click = new Button("Click me!");
       click.setAlignment(Pos.CENTER);
      click.setStyle("-fx-background-color:HotPink; -fx-font-weight:bold");

       HBox hbclick = new HBox(click);
       hbclick.setPrefHeight(800);
       hbclick.setPrefWidth(1000);
       hbclick.setAlignment(Pos.CENTER);
       
       int []clickarr = new int[]{0};

       click.setOnAction(new EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
            if(clickarr[0]%4==0){
               hbclick.setStyle("-fx-background-color:SandyBrown");
            }
            else if(clickarr[0]%4==1){
               hbclick.setStyle("-fx-background-color:Green");
            }
           else if(clickarr[0]%4==2){
               hbclick.setStyle("-fx-background-color:cyan");
            }
           else if(clickarr[0]%4==3){
               hbclick.setStyle("-fx-background-color:black");
            }
            clickarr[0]++;
         
         }
         
       }); 
      // Group gp = new Group(hbclick);
       Scene scene = new Scene(gp);       
       scene.setFill(Color.YELLOW);
       primaryStage.setScene(scene);
       primaryStage.show();
    }
    
}
