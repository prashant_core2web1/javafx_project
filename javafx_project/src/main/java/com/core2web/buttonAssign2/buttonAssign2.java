package com.core2web.buttonAssign2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class buttonAssign2 extends Application{
       @Override
    public void start(Stage buttonStage){
       buttonStage.setTitle("Core2Web");
       buttonStage.setHeight(800);
       buttonStage.setWidth(1000);
       buttonStage.getIcons().add(new Image("images/image.jpeg"));
       buttonStage.setResizable(false);
       //***************************************************************************************************/
         
       Button show = new Button("Hello Super-X");
       //Font boldBlackFont = Font.font("Arial",FontWeight.BOLD, 20);
       //show.setFont(boldBlackFont);
       show.setAlignment(Pos.CENTER);
       HBox hb = new HBox(show);
       hb.setPrefHeight(750);  
       hb.setPrefWidth(1000);
       hb.setAlignment(Pos.CENTER);
       
       show.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            System.out.println("Core2web");
           Font boldBlackFont = Font.font("Arial",FontWeight.BLACK, 14);
           show.setFont(boldBlackFont);
        }
        
       });
       
       //*******************************************************************************************************/
      
       Button showjava = new Button("Core2web-Java");
       showjava.setPrefWidth(180);
       showjava.setAlignment(Pos.CENTER);
       VBox vbjava = new VBox(showjava);
       vbjava.setPrefHeight(30);
       vbjava.setPrefWidth(300);
       vbjava.setAlignment(Pos.CENTER);
       
       Button showsuperX = new Button("Core2web-Super-X");
       showsuperX.setPrefWidth(180);
       showsuperX.setAlignment(Pos.CENTER);
       VBox vbsuperX = new VBox(showsuperX);
       vbsuperX.setPrefHeight(30);
       vbsuperX.setPrefWidth(300);
       vbsuperX.setAlignment(Pos.CENTER);

       Button showDSA = new Button("Core2web-DSA");
       showDSA.setPrefWidth(180);
       showDSA.setAlignment(Pos.CENTER);
       VBox vbDSA = new VBox(showDSA);
       vbDSA.setPrefHeight(30);
       vbDSA.setPrefWidth(300);
       vbDSA.setAlignment(Pos.CENTER);

       showjava.setStyle("-fx-background-color:Blue");
       showsuperX.setStyle("-fx-background-color:Blue");
       showDSA.setStyle("-fx-background-color:Blue");
       
       VBox vb2 = new VBox(10,vbjava,vbsuperX,vbDSA);
       vb2.setPrefHeight(800);
       vb2.setPrefWidth(1000);
       vb2.setAlignment(Pos.CENTER);
       
       Label lb = new Label("Core2web.in");
       lb.setFont(new Font(30));
       lb.setLayoutX(400);
       lb.setLayoutY(200);
       lb.setPrefWidth(200);
       Font boldBlackFont = Font.font("Arial",FontWeight.BOLD, 24);
       lb.setFont(boldBlackFont);
       lb.setAlignment(Pos.CENTER);

       showjava.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

        public void handle(ActionEvent event) {
           System.out.println("Java - 2024");
           showjava.setStyle("-fx-background-color:Green");
           Font boldBlackFont = Font.font("Arial",FontWeight.BOLD, 14);
           showjava.setFont(boldBlackFont);
        }
        
       });

       showsuperX.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

        public void handle(ActionEvent event) {
           System.out.println("SuperX - 2024");
           showsuperX.setStyle("-fx-background-color:Green");
           Font boldBlackFont = Font.font("Arial",FontWeight.BOLD, 14);
           showsuperX.setFont(boldBlackFont);
        }
        
       });

       showDSA.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

        public void handle(ActionEvent event) {
           System.out.println("DSA - 2024");
           showDSA.setStyle("-fx-background-color:Green");
           Font boldBlackFont = Font.font("Arial",FontWeight.BOLD, 14);
           showDSA.setFont(boldBlackFont);
        }
        
       });
       //**************************************************************************************************/
       Button bt1 = new Button("OS");
       bt1.setAlignment(Pos.CENTER);

       Button c = new Button("C");
       c.setAlignment(Pos.CENTER);

       Button cpp = new Button("CPP");
       cpp.setAlignment(Pos.CENTER);
   
       HBox hb3 = new HBox(20,bt1,c,cpp);
       hb3.setPrefHeight(30);
       hb3.setPrefWidth(300);
       hb3.setLayoutY(450);
       hb3.setLayoutX(350);
       hb3.setAlignment(Pos.CENTER);
       
       Button java = new Button("JAVA");
       java.setAlignment(Pos.CENTER);

       Button dsa = new Button("DSA");
       dsa.setAlignment(Pos.CENTER);

       Button python = new Button("Python");
       python.setAlignment(Pos.CENTER);
       
       Label lb1 = new Label("Core2web.in");
       lb1.setFont(new Font(30));
       lb1.setLayoutX(405);
       lb1.setLayoutY(200);
       lb1.setPrefWidth(200);
       Font boldBlackFont1 = Font.font("Arial",FontWeight.BOLD, 24);
       lb1.setFont(boldBlackFont1);
       lb1.setAlignment(Pos.CENTER);

       bt1.setStyle("-fx-background-color:Blue");
       c.setStyle("-fx-background-color:Blue");
       cpp.setStyle("-fx-background-color:Blue");
       java.setStyle("-fx-background-color:Blue");
       dsa.setStyle("-fx-background-color:Blue");
       python.setStyle("-fx-background-color:Blue");

       HBox hb4 = new HBox(20, java, dsa, python);
       hb4.setPrefHeight(40);
       hb4.setPrefWidth(300);
       hb4.setLayoutX(350);
       hb4.setLayoutY(500);
       hb4.setAlignment(Pos.CENTER);
       
      bt1.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
            System.out.println("Operating System");
            bt1.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");

         }
         
      });

      c.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
           System.out.println("C Language");
           c.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");
         }
         
      });
      
      cpp.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
           System.out.println("CPP Language");
           cpp.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");
         }
         
      });
      
      java.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
           System.out.println("JAVA Language");
           java.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");
         }
         
      });

      dsa.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
           System.out.println("DSA Language");
           dsa.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
         }
         
      });

      python.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

         public void handle(ActionEvent event) {
           System.out.println("PYTHON Language");
           python.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");
         }
         
      }); 
      
      //***********************************************************************************************/
       //Group gp = new Group(lb1,hb3,hb4);
       Group gp = new Group(hb);
       //Group gp = new Group(lb,vb2);
       Scene sc = new Scene(gp);
       sc.setFill(Color.YELLOW);
       buttonStage.setScene(sc);
       buttonStage.show();
}
}
